# wingspress

Tool for migrating Wings content to WordPress.

## Requirements

- [NodeJS](https://nodejs.org/)
- [Yarn](https://yarnpkg.com/)

## Pull in mobile-doc-wp-renderer
<!-- git clone git@gitlab.com:bij1/site/mobiledoc-wp-renderer.git -->
git submodule init
git submodule update

## Build mobile-doc-wp-renderer
```bash
cd mobiledoc-wp-renderer
yarn install
yarn build
cd ..
```

## `yarn install`

Install all the dependencies

## `yarn fetch`

Fetches all content from Wings and saves it to `content.json`,
requires setting environment variables, for example:

```
export WINGS_ENDPOINT=https://api.wings.dev
export WINGS_PROJECT=<PROJECT_ID>
export WINGS_APP_KEY=<APP_KEY>
```

These credentials can be generated by adding an App in the Wings admin
backend.

## `yarn migrate`

Converts all articles and pages that do not exist in WordPress yet and
publishes them along with their media, requires setting environment
variables, for example:

```
export WP_ENDPOINT=http://localhost:8000/wp-json
export WP_USER=<USERNAME>
export WP_PASSWORD=<APPLICATION_PASSWORD>
```

Note that using this API requires a specific application password
which can be generated from the user management page.
